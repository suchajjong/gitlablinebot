package com.jongzazaal.linebot.model.push

data class Source(
    var avatar_url: String? = "",
    var ci_config_path: String? = "",
    var default_branch: String? = "",
    var description: String? = "",
    var git_http_url: String? = "",
    var git_ssh_url: String? = "",
    var homepage: String? = "",
    var http_url: String? = "",
    var id: Int? = 0,
    var name: String? = "",
    var namespace: String? = "",
    var path_with_namespace: String? = "",
    var ssh_url: String? = "",
    var url: String? = "",
    var visibility_level: Int? = 0,
    var web_url: String? = ""
)