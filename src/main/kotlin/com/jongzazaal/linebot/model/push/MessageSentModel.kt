package com.jongzazaal.linebot.model.push

data class MessageSentModel(
        var message: String? = "",
)
