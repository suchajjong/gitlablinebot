package com.jongzazaal.linebot.model.push

data class User(
    var avatar_url: String? = "",
    var email: String? = "",
    var id: Int? = 0,
    var name: String? = "",
    var username: String? = ""
)