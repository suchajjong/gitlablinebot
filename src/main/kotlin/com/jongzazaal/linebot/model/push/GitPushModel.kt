package com.jongzazaal.linebot.model.push

data class GitPushModel(
    var after: String? = "",
    var before: String? = "",
    var checkout_sha: String? = "",
    var commits: List<Commit>? = listOf(),
    var object_kind: String? = "",
    var project: Project? = Project(),
    var project_id: Int? = 0,
    var ref: String? = "",
    var repository: Repository? = Repository(),
    var total_commits_count: Int? = 0,
    var user_avatar: String? = "",
    var user_email: String? = "",
    var user_id: Int? = 0,
    var user_name: String? = "",
    var user_username: String? = ""
)
data class GitMergeModel(

        var object_kind: String? = "",
        var project: Project? = Project(),
        var user: User? = User(),
        var object_attributes: ObjectAttributes? = ObjectAttributes(),
)

data class GitPipelineModel(

        var object_kind: String? = "",
        var project: Project? = Project(),

        var object_attributes: ObjectAttributesPipeline? = ObjectAttributesPipeline(),
)
