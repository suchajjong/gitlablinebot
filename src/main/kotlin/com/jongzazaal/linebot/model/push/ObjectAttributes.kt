package com.jongzazaal.linebot.model.push

data class ObjectAttributes(
    var action: String? = "",
    var author_id: Int? = 0,
    var created_at: String? = "",
    var description: String? = "",
    var merge_status: String? = "",
    var source: Source? = Source(),
    var source_branch: String? = "",
    var state: String? = "",
    var target: Target? = Target(),
    var target_branch: String? = "",
    var title: String? = "",
    var updated_at: String? = "",
    var url: String? = "",
    var status: String? = "",
    var stages: List<String>? = arrayListOf()
)

data class ObjectAttributesPipeline(
        var ref: String? = "",
        var source: String? = "",
        var status: String? = "",
        var created_at: String? = "",
        var finished_at: String? = "",
        var duration: Int? = null,

        var stages: List<String>? = arrayListOf()
)