package com.jongzazaal.linebot.model.push

data class Commit(
    var added: List<String>? = listOf(),
    var author: Author? = Author(),
    var id: String? = "",
    var message: String? = "",
    var modified: List<String>? = listOf(),
    var removed: List<String>? = listOf(),
    var timestamp: String? = "",
    var title: String? = "",
    var url: String? = ""
)