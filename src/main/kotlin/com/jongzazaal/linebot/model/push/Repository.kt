package com.jongzazaal.linebot.model.push

data class Repository(
    var description: String? = "",
    var git_http_url: String? = "",
    var git_ssh_url: String? = "",
    var homepage: String? = "",
    var name: String? = "",
    var url: String? = "",
    var visibility_level: Int? = 0
)