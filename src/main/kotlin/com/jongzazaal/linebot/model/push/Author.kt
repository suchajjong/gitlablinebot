package com.jongzazaal.linebot.model.push

data class Author(
    var email: String? = "",
    var name: String? = ""
)