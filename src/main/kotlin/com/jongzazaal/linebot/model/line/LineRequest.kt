package com.jongzazaal.linebot.model.line

data class LineRequest(
        var msg: String? = ""
)