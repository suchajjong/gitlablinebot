package com.jongzazaal.linebot.model.line

data class LineNotifyResponse(
        var status: Int? = 0,
        var message: String? = "",
)
