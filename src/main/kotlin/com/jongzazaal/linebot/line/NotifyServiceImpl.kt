package com.jongzazaal.linebot.line

import com.jongzazaal.linebot.key.BotKey
import com.jongzazaal.linebot.model.line.LineNotifyResponse
import com.jongzazaal.linebot.model.line.LineRequest
import com.jongzazaal.linebot.model.push.GitMergeModel
import com.jongzazaal.linebot.model.push.GitPipelineModel
import com.jongzazaal.linebot.model.push.GitPushModel
import com.jongzazaal.linebot.model.push.MessageSentModel
import com.jongzazaal.linebot.utility.DateUtility
import com.jongzazaal.linebot.utility.toFormatZeroLeft
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.util.LinkedHashMap

import java.lang.Exception


@Service
class NotifyServiceImpl {
    private val lineUrl = URI.create("https://notify-api.line.me/api/notify")
    private val contentType = "application/x-www-form-urlencoded"
    fun pushNotify(lineToken: String, pushEvent: GitPushModel?): ResponseEntity<String> {
        val message = "[${pushEvent?.object_kind}]${pushEvent?.repository?.name}"
        val data = java.lang.String.format("message=%s", message+"//"+pushEvent)
        return httpPost(lineToken, data)
    }

    private fun getMessageMergeRequest(mergeRequestEvent: GitMergeModel?): String{
        val  projectName = mergeRequestEvent?.project?.name?:"projectName"
        val  userName = mergeRequestEvent?.user?.name?:"userName"
        val sourceBranch = mergeRequestEvent?.object_attributes?.source_branch?:"source_branch"
        val targetBranch = mergeRequestEvent?.object_attributes?.target_branch?:"target_branch"
        val title = mergeRequestEvent?.object_attributes?.title?:"-"
        val des = mergeRequestEvent?.object_attributes?.description?:"-"
        val line ="----------"

        val url = mergeRequestEvent?.object_attributes?.url?:""
        var text = """[Merge Request]
            |[$projectName]
            |$line
            |send by: $userName
            |$line
            |title: $title
            |$line
            |des: $des
            |$line
            |${sourceBranch}->${targetBranch}
            |$line
            |${url}""".trimMargin()
        return text
    }
    private fun getMessagePipeline(mEvent: GitPipelineModel?): String{
        val  projectName = mEvent?.project?.name?:"projectName"
        val statusPipeline = mEvent?.object_attributes?.status
        val refPipeline = mEvent?.object_attributes?.ref
        val sourcePipeline = mEvent?.object_attributes?.source

        var duration: String? = null

        mEvent?.object_attributes?.duration?.let {
            val(hours, minutes, seconds) = DateUtility.getTimeByMillisecond(it*1000L)
            duration="${hours.toFormatZeroLeft(2)}:${minutes.toFormatZeroLeft(2)}:${seconds.toFormatZeroLeft(2)}"

        }

        val line ="----------"

        var text = """[Pipeline]
            |[$projectName]
            |$line
            |ref : $refPipeline
            |$line
            |duration: $duration
            |$line
            |status: $statusPipeline
            |$line
            """.trimMargin()
        return text
    }

//    fun pushTagNotify(lineToken: String, pushTagEvent: PushTagEvent?): ResponseEntity<String> {
//        val data = java.lang.String.format("message=%s", pushTagEvent)
//        return httpPost(lineToken, data)
//    }
//
//    fun issueNotify(lineToken: String, issueEvent: IssueEvent?): ResponseEntity<String> {
//        val data = java.lang.String.format("message=%s", issueEvent)
//        return httpPost(lineToken, data)
//    }
//
    fun mergeRequestNotify(lineToken: String, mergeRequestEvent: GitMergeModel?, type: String): LineNotifyResponse? {
        val text = getMessageMergeRequest(mergeRequestEvent)
        return when(type){
            BotKey.SEND_TYPE_LINE -> {callLineNotify(sendLineNotifyMessages(text),lineToken)}
            BotKey.SEND_TYPE_WORKPLACE -> {
                val linkMerge = mergeRequestEvent?.object_attributes?.url?:""
                callWorkPlaceNotify(sendWorkPlaceNotifyMessages(text, linkMerge), lineToken)
            }
            else -> {callDiscordNotify(sendDiscordNotifyMessages(text), lineToken)}
        }
    }
    fun pipelineNotify(lineToken: String, mEvent: GitPipelineModel?, type: String): LineNotifyResponse? {
        val text = getMessagePipeline(mEvent)
        return when(type){
            BotKey.SEND_TYPE_LINE -> {callLineNotify(sendLineNotifyMessages(text),lineToken)}
            else -> {callDiscordNotify(sendDiscordNotifyMessages(text), lineToken)}
        }
    }

    fun callLineMsg(lineToken: String, msg: LineRequest?): LineNotifyResponse? {

        return callLineNotify(sendLineNotifyMessages(msg?.msg?:"-"),lineToken)
    }
    fun callDiscordMsg(lineToken: String, msg: LineRequest?): LineNotifyResponse? {

        return callDiscordNotify(sendDiscordNotifyMessages(msg?.msg?:"-"),lineToken)
    }


    @Throws(Exception::class)
    fun sendLineNotifyMessages(msg: String?): MultiValueMap<String, Any> {
        val map: MultiValueMap<String, Any> = LinkedMultiValueMap()
        map.add("message", msg)
        return map
    }
    @Throws(Exception::class)
    fun sendDiscordNotifyMessages(msg: String?): MultiValueMap<String, Any> {
        val map: MultiValueMap<String, Any> = LinkedMultiValueMap()
        map.add("content", msg)
        return map
    }
    @Throws(Exception::class)
    fun sendWorkPlaceNotifyMessages(msg: String?, linkMerge: String): MultiValueMap<String, Any> {
        val map: MultiValueMap<String, Any> = LinkedMultiValueMap()
        map.add("message", msg)
        map.add("link", linkMerge)
        return map
    }

    fun httpPost(lineToken: String, data: String): ResponseEntity<String> {
        val bearerAuth = "Bearer $lineToken"
        val headers: MultiValueMap<String, String> = LinkedMultiValueMap()
        headers.add("Content-Type", contentType)
        headers.add("Content-Length", "" + data.toByteArray().size)
        headers.add("Authorization", bearerAuth)
        val request: RequestEntity<*> = RequestEntity<Any?>(
                data, headers, HttpMethod.POST, lineUrl)
        val restTemplate = RestTemplate()
        return restTemplate.exchange(request, String::class.java)
    }

    @Throws(Exception::class)
    private fun callLineNotify(map: MultiValueMap<String, Any>, lineToken: String): LineNotifyResponse? {
        val restTemplate = RestTemplate()
        val headers = HttpHeaders()

        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
        headers.add("Authorization", "Bearer $lineToken")
        headers.add("Content-Type", contentType)

        val request = HttpEntity(map, headers)
        return restTemplate.postForObject("https://notify-api.line.me/api/notify", request, LineNotifyResponse::class.java)
    }

    @Throws(Exception::class)
    private fun callDiscordNotify(map: MultiValueMap<String, Any>, urlWebHook: String): LineNotifyResponse? {
        val restTemplate = RestTemplate()
        val headers = HttpHeaders()

        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
        headers.add("Content-Type", contentType)
        headers.add("user-agent", "Application")

        val request = HttpEntity(map, headers)
        return restTemplate.postForObject(urlWebHook, request, LineNotifyResponse::class.java)
    }

    @Throws(Exception::class)
    private fun callWorkPlaceNotify(map: MultiValueMap<String, Any>, urlWebHook: String): LineNotifyResponse? {
        val restTemplate = RestTemplate()
        val headers = HttpHeaders()

        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED)
        headers.add("Content-Type", contentType)
        headers.add("user-agent", "Application")

        val request = HttpEntity(map, headers)
        return restTemplate.postForObject(urlWebHook, request, LineNotifyResponse::class.java)
    }
}
