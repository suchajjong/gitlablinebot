package com.jongzazaal.linebot.utility

object DateUtility {
    fun getTimeByMillisecond(millisecond: Long): Triple<Long, Long, Long>{
        val hours = (millisecond/1000) / 3600
        val minutes = (millisecond/1000) % 3600 / 60
        val seconds = (millisecond/1000) % 60
        return Triple(hours, minutes, seconds)
    }
}

fun Long?.toFormatZeroLeft(length: Int):String{
    return(String.format("%0${length}d", this))
}