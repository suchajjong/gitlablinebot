package com.jongzazaal.linebot

import com.jongzazaal.linebot.key.BotKey
import com.jongzazaal.linebot.line.NotifyServiceImpl
import com.jongzazaal.linebot.model.line.LineRequest
import com.jongzazaal.linebot.model.push.GitMergeModel
import com.jongzazaal.linebot.model.push.GitPipelineModel
import com.jongzazaal.linebot.model.push.GitPushModel
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@SpringBootApplication
@RestController
class LinebotApplication{
	val noti = NotifyServiceImpl()
	//9ImvPacAYsv7BcwGbAwlSMFfpVe27bUwYautkmXIEQ5
	@GetMapping("/hello")
	fun hello(@RequestParam(value = "name", defaultValue = "World") name: String?): String? {
		return String.format("Hello %s!", name)
	}

//	@PostMapping("/pushAuther")
//	fun pushNotify(@RequestBody data: Author?): ResponseEntity<*>? {
//		println(data?.toString())
//		return ResponseEntity.ok(data?:Author())
//	}

	@PostMapping("/gitlab-punpro-webhook/push")
	fun pushNotify(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitPushModel?): ResponseEntity<*>? {
		println(data?.toString())
		noti.pushNotify(line_token, data)
		return ResponseEntity.ok(data?:GitPushModel())
	}

	@PostMapping("/gitlab-punpro-webhook/merge-request")
	fun mergeRequestNotify(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitMergeModel?): ResponseEntity<*>? {
		println(data?.toString())
		noti.mergeRequestNotify(line_token, data, BotKey.SEND_TYPE_LINE)
		return ResponseEntity.ok(data?:GitMergeModel())
	}

	@PostMapping("/line/send-msg")
	fun sendMsgRequestNotify(@RequestHeader(value = "line-token")line_token: String, @RequestBody data: LineRequest?): ResponseEntity<*>? {
		println(data?.toString())
		noti.callLineMsg(line_token, data)
		return ResponseEntity.ok(data?:GitMergeModel())
	}

	@PostMapping("/discord/send-msg")
	fun sendMsgRequestNotifyDiscord(@RequestHeader(value = "discord-token")line_token: String, @RequestBody data: LineRequest?): ResponseEntity<*>? {
		println(data?.toString())
		noti.callDiscordMsg(line_token, data)
		return ResponseEntity.ok(data?:GitMergeModel())
	}
	@PostMapping("/gitlab-punpro-webhook/list/merge-request")
	fun mergeRequestListNotify(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitMergeModel?): ResponseEntity<*>? {
		val listLines = line_token.replace("\\s".toRegex(), "").split(",")
		if(data?.object_attributes?.state == "opened" && data.object_attributes?.action == "open"){
			for (i in listLines){
				noti.mergeRequestNotify(i, data, BotKey.SEND_TYPE_LINE)
			}
		}
		return ResponseEntity.ok(data?:GitMergeModel())
	}

	@PostMapping("/gitlab-punpro-webhook/list/pipeline")
	fun pipelineListNotify(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitPipelineModel?): ResponseEntity<*>? {
		val listLines = line_token.replace("\\s".toRegex(), "").split(",")
//		println(line_token)
//		println(listLines)
//		if(data?.object_attributes?.state == "opened" && data.object_attributes?.action == "open"){
			for (i in listLines){
				noti.pipelineNotify(i, data, BotKey.SEND_TYPE_LINE)
			}
//		}
		return ResponseEntity.ok(data?:GitPipelineModel())
	}

	@PostMapping("/gitlab-punpro-webhook/discord/list/merge-request")
	fun mergeRequestListNotifyDiscord(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitMergeModel?): ResponseEntity<*>? {
		val listLines = line_token.replace("\\s".toRegex(), "").split(",")
		if(data?.object_attributes?.state == "opened" && data.object_attributes?.action == "open"){
			for (i in listLines){
				noti.mergeRequestNotify(i, data, BotKey.SEND_TYPE_DISCORD)
			}
		}
		return ResponseEntity.ok(data?:GitMergeModel())
	}

	@PostMapping("/gitlab-punpro-webhook/discord/list/pipeline")
	fun pipelineListNotifyDiscord(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitPipelineModel?): ResponseEntity<*>? {
		val listLines = line_token.replace("\\s".toRegex(), "").split(",")
		for (i in listLines){
			noti.pipelineNotify(i, data, BotKey.SEND_TYPE_DISCORD)
		}
		return ResponseEntity.ok(data?:GitPipelineModel())
	}

	@PostMapping("/gitlab-punpro-webhook/work-place/list/merge-request")
	fun mergeRequestListNotifyWorkPlace(@RequestHeader(value = "X-Gitlab-Token")line_token: String, @RequestBody data: GitMergeModel?): ResponseEntity<*>? {
		val listLines = line_token.replace("\\s".toRegex(), "").split(",")
		if(data?.object_attributes?.state == "opened" && data.object_attributes?.action == "open"){
			val targetBranch = data.object_attributes?.target_branch?:"target_branch"
			if(targetBranch == "master" || targetBranch == "production"){
				for (i in listLines){
					noti.mergeRequestNotify(i, data, BotKey.SEND_TYPE_WORKPLACE)
				}
			}
		}
		return ResponseEntity.ok(data?:GitMergeModel())
	}
}

fun main(args: Array<String>) {
	runApplication<LinebotApplication>(*args)
}
