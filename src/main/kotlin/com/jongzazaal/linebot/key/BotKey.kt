package com.jongzazaal.linebot.key

object BotKey {
    val SEND_TYPE_LINE = "send_type_line"
    val SEND_TYPE_DISCORD = "send_type_discord"
    val SEND_TYPE_WORKPLACE = "send_type_workplace"

}